package de.esel.cardsau.interval;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.RadioButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.view.View;
import android.view.View.OnClickListener;

import android.net.Uri;
import android.media.RingtoneManager;
import android.media.Ringtone;
import android.widget.ToggleButton;

import java.util.List;
import java.util.ArrayList;

/**
 * @brief A small application to trigger sport intervals with a setup time and active time.
 *
 * TODO Ee want a non-volatile app preference for all default values
 *
 * TODO Support multiple configurations and a configuration manager/selector
 */
public class MainActivity extends AppCompatActivity
{
    /**
     * @brief Get a reference on all UI elements and read out the default ringtone from the system
     * 
     * @param savedInstanceState
     */
    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        // Set the current view
        setContentView( R.layout.activity_main );

        // Read the UI element references
        m_setupTimeText = findViewById( R.id.time1 );
        m_holdTimeText  = findViewById( R.id.time2 );
        m_iteratorText  = findViewById( R.id.iterator );

        m_startButton   = findViewById( R.id.startButton );
        m_soundButton   = findViewById( R.id.sound );
        m_progressBar   = findViewById( R.id.progressBar );

        // Read the default ringtone from the system
        m_ringtone      = RingtoneManager.getRingtone(this, Settings.System.DEFAULT_NOTIFICATION_URI );

        // Write the ringtone title to the  sound selection button
        m_soundButton.setText( m_ringtone.getTitle( this ) );

        addListenerOnButton();
    }

    /**
     * @brief Handle start stop button events
     */
    public void addListenerOnButton() {

        // Add the start button on click event listener
        m_startButton.setOnClickListener
        (
            new OnClickListener()
            {
                @Override
                public void onClick( View arg0 )
                {
                    // The startButton stores the program state
                    if ( m_startButton.isChecked() )
                    {
                        start();
                    }
                    else
                    {
                        stop();
                    }
                }
            }
        );

        // Add the sound button on click event listener
        m_soundButton.setOnClickListener(new OnClickListener()
        {
            public void onClick(View arg0)
            {
                // Setup the notification tone selection dialog
                final Uri currentTone= RingtoneManager.getActualDefaultRingtoneUri
                (
                    MainActivity.this,
                     RingtoneManager.TYPE_ALARM
                );


                Intent intent = new Intent( RingtoneManager.ACTION_RINGTONE_PICKER );
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION );
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, currentTone );
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true );
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true );

                // Start the dialog
                startActivityForResult( intent, RESULT_OK );
            }
        });
    }

    /**
     * @brief Handle the ringtone selection result
     *
     * @param requestCode Requested result code
     * @param resultCode  Actual result code
     * @param data        Result data
     *
     * TODO handle empty selection to support silent intervals
     */
    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data)
    {
        // Do not
        if ( resultCode != RESULT_OK ) return;

        Uri uri = data.getParcelableExtra( RingtoneManager.EXTRA_RINGTONE_PICKED_URI );

        if ( uri == null ) return;

        m_ringtone = RingtoneManager.getRingtone(this, uri );

        m_soundButton.setText( m_ringtone.getTitle( this ) );
    }

    /**
     * @brief Start the configured  timer intervals
     *
     * @details Read the values from the settings fields, store the time steps into an array and
     * calculate the progress bar settings.
     * Afterwards the first delay timer is setup and activated.
     */
    private void start()
    {
        try
        {
            // Read the times from the UI and convert them to milliseconds integer values
            Integer setupTime  = ( int )( Float.parseFloat( m_setupTimeText.getText().toString() ) * 1000.0 );
            Integer holdTime   = ( int )( Float.parseFloat( m_holdTimeText .getText().toString() ) * 1000.0 );

            Integer iterations = Integer.parseInt( m_iteratorText.getText().toString() );

            // Reset the progress bar and set the maximal values based on the configuration
            m_progressBar.setMax( ( setupTime + holdTime ) * iterations );
            m_progressBar.setProgress( 0 );

            // Write all timer steps into an array
            m_timings = new ArrayList<>();

            for ( int i = 0; i < iterations; ++i )
            {
                // Add the setup time
                m_timings.add( setupTime );

                // Add the hold time
                m_timings.add( holdTime );
            }

            // Start the first setup time delay counter
            m_Handler.postDelayed( m_Runnable,  m_timings.get( 0 )  );
        }
        catch ( NumberFormatException e )
        {
            stop();
        }
    }

    /**
     * @brief Stop the interval by unchecking the start button
     */
    private void stop()
    {
        m_startButton.setChecked( false );
    }

    /**
     * @brief Handle the next timer event
     *
     * @details Stop the interval if the stop button was activated. Else play the selected
     * notification tone and increment the progress bar. In case there are timer steps left in the
     * timing array the next timer event is activate. Else the interval is stopped.
     */
    private void nextStep()
    {
        // Check the start button state for stop and interrupt the timer chain
        if ( ! m_startButton.isChecked() ) return;

        // Play the selected tone
        beep();

        // Update the progress bar with the value of the current timer
        m_progressBar.incrementProgressBy( m_timings.get( 0 ) );

        // Remove the current time from the timings list
        m_timings.remove( 0 );

        // Check for a next time in the timings list and setup the delay timer with it
        if ( m_timings.size() > 0 )
        {
            m_Handler.postDelayed( m_Runnable, m_timings.get( 0 ) );
        }
        // Stop the interval if no timing is left in the list
        else
        {
            stop();
        }
    }

    /**
     * @brief Play the selected ringtone sound.
     */
    private void beep()
    {
        try
        {
            m_ringtone.play();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /** Reference to the setup time input value test field */
    TextView         m_setupTimeText;

    /** Reference to the hold time input value test field */
    TextView         m_holdTimeText;

    /** Reference to the iteration count input value test field */
    TextView         m_iteratorText;

    /** Reference to the start/stop toggle button */
    ToggleButton     m_startButton;

    /** Reference to the ringtone selection radio button */
    RadioButton      m_soundButton;

    /** Reference to the progress bar */
    ProgressBar      m_progressBar;

    /** List of timings remaining in the interval */
    List< Integer >  m_timings;

    /** Currently active ringtone to avoid reselection during active interval */
    Ringtone         m_ringtone;

    /** Delay timer event handler */
    private Handler  m_Handler  = new Handler();

    /** Delay timer event callable */
    private Runnable m_Runnable = new Runnable()
    {
        public void run()
        {
            nextStep();
        }
    };

}
